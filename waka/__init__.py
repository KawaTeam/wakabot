# ------------------------------------------------------------------------------
#  Copyleft 2017-2021  PacMiam
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

class WakaMetadata(object):

    NAME = "WakaBot"
    VERSION = "0.5.0"
    CODENAME = "Huehuetenango"
    LICENSE = "GNU/GPLv3"

    MODULE_API_VERSION = "0.3.0"

    DESCRIPTION = "WakaBot is another stupid IRC bot made just for fun"

    SHORTDESCRIPTION = f"{NAME} v.{VERSION} - License {LICENSE}"

    EPILOG = f"Copyleft Kawa-Team - License {LICENSE}"
