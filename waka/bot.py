# ------------------------------------------------------------------------------
#  Copyleft 2017-2021  PacMiam
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Configuration
from configparser import ConfigParser

# Filesystem
from pathlib import Path

# Network
from ssl import SSLContext, CERT_REQUIRED
from socket import gaierror, socket, AF_INET, SOCK_STREAM

# Logging
from logging import getLogger, Formatter, FileHandler, StreamHandler

# Regex
from re import compile as re_compile

# System
from sys import version_info
from sys import exit as sys_exit
from sys import path as sys_path

from importlib import import_module
from importlib import reload as reload_module

# Thread
from threading import Timer

# Time
from time import sleep

# Waka
from waka import WakaMetadata
from waka.irc import IRC
from waka.weblistener import WakabotHTTPServerThread
from waka.utils import Version


class Bot(object):

    def __init__(self, configuration, debug=False):
        """ Constructor

        Parameters
        ----------
        configuration : str
            Configuration file path
        debug : bool, default: False
            Debug flag

        Raises
        ------
        FileNotFoundError
            when specified configuration not exists
        IsADirectoryError
            when specified configuration is a directory
        """

        self.path = Path(configuration).expanduser().resolve()

        if not self.path.exists():
            raise FileNotFoundError(
                "Cannot found configuration file", str(self.path))

        elif self.path.is_dir():
            raise IsADirectoryError(
                "Specified path is not a file", str(self.path))

        # Debug flag for logger instance
        self.flag_debug = debug
        # Configuration object
        self.__configuration = ConfigParser()

        # Bot options
        self.__options = dict()
        # Bot messages
        self.__messages = dict()
        # Bot status
        self.__is_alive = True

        # Users
        self.__users = dict()
        # Modules
        self.__modules = dict()
        # Kick cache
        self.__kick = dict()
        # Joined channels
        self.joined_channels = list()

        # Web HTTP listener
        self.listener_thread = None

        # IRC regex patterns
        self.__regex = [
            (re_compile(IRC.Patterns.PING), self.on_ping_request),
            (re_compile(IRC.Patterns.ERROR), self.on_error_request),
            (re_compile(IRC.Patterns.USERS), self.on_list_request),
            (re_compile(IRC.Patterns.INFORMATION), self.on_replies_request),
            (re_compile(IRC.Patterns.REPLIES), self.on_replies_request),
            (re_compile(IRC.Patterns.NICK), self.on_nick_request),
            (re_compile(IRC.Patterns.JOIN), self.on_join_request),
            (re_compile(IRC.Patterns.KICK), self.on_kick_request),
            (re_compile(IRC.Patterns.PART), self.on_part_request),
            (re_compile(IRC.Patterns.QUIT), self.on_quit_request),
            (re_compile(IRC.Patterns.PRIVMSG), self.on_private_message_request),
        ]

        # Commands
        self.__commands = {
            "exit": {
                "usage": "Terminate bot existence",
                "function": self.command_exit,
            },
            "version": {
                "usage": "Show bot version",
                "function": self.command_version,
            },
            "help": {
                "usage": "Show this help message",
                "function": self.command_help,
            },
            "reload": {
                "usage": "Reload modules list or a specific module",
                "function": self.command_reload,
            },
        }

        # Initialize program
        self.__init_vars()
        self.__init_logger()

    def __init_vars(self):
        """ Initialize variable from specified configuration file

        Raises
        ------
        KeyError
            when server address is missing
            when bot name is missing
        """

        data = {
            "bot": {
                "admin": {
                    "type": list,
                    "fallback": str(),
                },
                "name": {
                    "type": str,
                    "fallback": "Wakabot",
                },
                "password": {
                    "type": str,
                    "fallback": str(),
                },
            },
            "channel": {
                "join": {
                    "type": list,
                    "fallback": str(),
                },
            },
            "irc": {
                "port": {
                    "type": int,
                    "fallback": 6697,
                },
                "server": {
                    "type": str,
                    "fallback": "irc.libera.chat",
                },
                "use-ssl": {
                    "type": bool,
                    "fallback": True,
                },
            },
            "modules": {
                "exclude-users": {
                    "type": list,
                    "fallback": "ChanServ",
                },
                "ignores": {
                    "type": list,
                    "fallback": str(),
                },
            },
            "options": {
                "auto-rejoin": {
                    "type": bool,
                    "fallback": False,
                },
                "character": {
                    "type": str,
                    "fallback": "!",
                },
                "kick-timeout": {
                    "type": int,
                    "fallback": 10,
                },
            },
            "path": {
                "log": {
                    "type": Path,
                    "fallback": "waka.log",
                },
                "modules": {
                    "type": list,
                    "fallback": str(),
                },
            },
            "listener": {
                "active": {
                    "type": bool,
                    "fallback": False,
                },
                "port": {
                    "type": int,
                    "fallback": 9876,
                },
            },
        }

        self.__configuration.read([self.path], encoding="utf8")

        for section, options in data.items():
            for option, values in options.items():

                if values["type"] is int:
                    value = self.__configuration.getint(
                        section, option, fallback=values["fallback"])

                elif values["type"] is bool:
                    value = self.__configuration.getboolean(
                        section, option, fallback=values["fallback"])

                else:
                    value = self.__configuration.get(
                        section, option, fallback=values["fallback"])

                    if values["type"] is list:
                        value = value.split()

                    elif issubclass(values["type"], Path):
                        value = Path(value).expanduser()

                setattr(self, f"{section}_{option.replace('-', '_')}", value)

        # Avoid to connect to empty server
        if not self.irc_server:
            raise KeyError("Missing server address")

        # Avoid to have a bot without any name
        if not self.bot_name:
            raise KeyError("Missing bot name")

        # Ignore message from bot when using modules
        if self.bot_name not in self.modules_exclude_users:
            self.modules_exclude_users.append(self.bot_name)

        # Avoid to have a negative or null value for timeout
        if self.options_kick_timeout <= 0:
            self.options_kick_timeout = 1

        # Retrieve special bot messages
        self.__messages.clear()
        for key in self.__configuration["messages"]:
            self.__messages[key] = self.__configuration.get("messages", key)

        # Retrieve modules path
        self.path_modules = [
            Path(module).expanduser() for module in self.path_modules]
        self.path_modules.append(
            Path(__file__).resolve().parent.parent.joinpath("modules"))

    def __init_logger(self):
        """ Initialize logger

        Create a logger object based on logging library
        """

        self.logger_level = "INFO"
        if self.flag_debug:
            self.logger_level = "DEBUG"

        self.logger = getLogger(__name__)
        self.logger.setLevel(self.logger_level)

        self.logger_stream_handler = StreamHandler()
        self.logger_stream_handler.setLevel(self.logger_level)

        self.logger_stream_formatter = Formatter(
            "%(levelname)-9s [%(threadName)s] [%(module)s] %(message)s")

        self.logger_stream_handler.setFormatter(self.logger_stream_formatter)
        self.logger.addHandler(self.logger_stream_handler)

        self.logger_file_handler = FileHandler(self.path_log)
        self.logger_file_handler.setLevel(self.logger_level)

        self.logger_file_formatter = Formatter(
            "%(asctime)s %(levelname)9s %(message)s")

        self.logger_file_handler.setFormatter(self.logger_file_formatter)
        self.logger.addHandler(self.logger_file_handler)

    def __init_modules(self):
        """ Initialize modules

        Returns
        -------
        bool
            True if initialization has been done correctly, False otherwise
        """

        self.logger.info("Start modules initialization")

        self.__modules.clear()

        for path in self.path_modules:

            if not path.exists():
                self.logger.warning(f"Cannot import module path '{str(path)}'")
                continue

            sys_path.insert(1, str(path))

            self.logger.info(f"Successfully add modules path '{str(path)}'")

            for module in path.glob('*'):
                manifest = module.joinpath('manifest.conf')

                if not module.is_dir():
                    continue

                if not manifest.exists():
                    self.logger.warning(
                        f"Cannot found 'manifest.conf' for module "
                        f"'{module.name}'")
                    continue

                try:
                    self.__init_module(manifest)

                except Exception:
                    self.logger.exception(
                        f"Cannot init module '{module.name}'")

    def __init_module(self, path):
        """ Initialize a module from his path

        Parameters
        ----------
        path : str
            Module manifest path

        Returns
        -------
        bool
            True if the module has been correctly loaded, False otherwise
        """

        config = ConfigParser()
        config.read([path], encoding="utf8")

        # Check if module manifest is okay
        if not config.has_section('plugin'):
            self.logger.warning(
                f"'plugin' section is missing in '{str(path)}'")
            return False

        name = config.get('plugin', 'name', fallback=str())

        if name in self.modules_ignores:
            self.logger.warning(f"Module '{name}' ignored")
            return True

        if not name:
            self.logger.error(f"Cannot use an empty name for '{str(path)}'")
            return False

        if name in self.__modules:
            self.logger.error(f"Module '{name}' already loaded")
            return False

        command = config.get("command", "name", fallback=name)

        if command in self.__commands.keys():
            self.logger.error(
                f"Command conflict between '{name}' and '{command}'")
            return False

        version = config.get("plugin", "version", fallback=None)

        if version is None:
            self.logger.error(f"Version value is missing in module '{name}'")
            return False

        module_version = Version(version)
        wakabot_version = Version(WakaMetadata.MODULE_API_VERSION)

        if not (module_version.major == wakabot_version.major and
           module_version.minor == wakabot_version.minor):
            self.logger.warning(f"Module version of '{name}' ({version}) is "
                                f"not compatible with the minimal module API "
                                f"version ({WakaMetadata.MODULE_API_VERSION})")
            return False

        module = self.load_module(name, path)

        if module is not None:
            self.__modules[command] = (path, module)

            for alias in module.conf_command_aliases:

                if alias in self.__commands.keys():
                    self.logger.error(
                        f"Command conflict between '{name}' and '{alias}'")
                    return False

                self.__modules[alias] = command

            self.logger.info(f"Successfully add module '{name}'")

    def __init_listener(self):
        """ Initialize the web HTTP listener if set as active in configuration
        """

        if self.listener_active:
            self.logger.info("Prepare web HTTP listener")
            self.listener_thread = WakabotHTTPServerThread(
                self, self.listener_port)
            self.listener_thread.daemon = True

            self.logger.info(
                f"Start the web HTTP listener on port {self.listener_port}")
            self.listener_thread.start()

    def __init_irc_socket(self):
        """ Initialize the IRC socket based on SSL option value

        When the SSL connection is needed, the socket must be wrap into a
        SSL context to validate the connection

        Returns
        -------
        socket.socket
            Initialized socket
        """

        if self.irc_use_ssl:
            self.logger.info("Generate the SSL context")

            self.ssl_context = SSLContext()

            self.ssl_context.verify_mode = CERT_REQUIRED
            self.ssl_context.check_hostname = True
            self.ssl_context.load_default_certs()

            self.context_ciphers = sorted(self.ssl_context.get_ciphers(),
                                          key=lambda element: element["name"])

            if self.flag_debug:
                for cipher in self.context_ciphers:
                    self.logger.debug("+ {name} ({protocol}) [{strength_bits}]"
                                      "".format(**cipher))

            self.logger.info(
                f"{len(self.context_ciphers)} ciphers were successfully added")

            return self.ssl_context.wrap_socket(
                socket(AF_INET, SOCK_STREAM), server_hostname=self.irc_server)

        return socket(AF_INET, SOCK_STREAM)

    def needs_administrator_permission(function):
        """ Check if specified user is an administrator and can run the function

        Returns
        -------
        tuple
            An error tuple if not an administrator, function results otherwise
        """

        def __function__(self, channel, name, data):
            if name not in self.bot_admin:
                self.send_private_message(name, self.__messages["not-admin"])
                return (False, )

            return function(self, channel, name, data)

        return __function__

    def load_module(self, name, path):
        """ Load dynamically a module from a specific command

        Parameters
        ----------
        name : str
            Module name
        path : pathlib.path
            Module path

        Returns
        -------
        waka.WakaModule or None
            Initialized module
        """

        try:
            module = import_module(f"{name}.plugin")

            reload_module(module)

            return module.Module(self.bot_name, self.options_character, path)

        except Exception:
            self.logger.exception(f"Cannot load module '{name}'")

        return None

    def main_loop(self):
        """ Initialize main loop
        """

        self.logger.info(f"Use Python interpreter version {version_info.major}"
                         f".{version_info.minor}.{version_info.micro}")

        self.logger.info(f"Use WakaBot version {WakaMetadata.VERSION}")

        # Initialize variables from configuration file
        self.__init_modules()

        # ----------------------------------------
        #   Connection
        # ----------------------------------------

        self.__init_listener()

        self.irc_socket = self.__init_irc_socket()

        self.logger.info(
            f"Start socket connection to {self.irc_server}:{self.irc_port}")

        try:
            self.irc_socket.connect((self.irc_server, self.irc_port))

        except KeyboardInterrupt:
            self.logger.warning("Close by keyboard interrupt")
            sys_exit()

        except ConnectionRefusedError:
            self.logger.error("Remote connection has been refused")
            sys_exit()

        except gaierror as error:
            self.logger.error(error)
            sys_exit()

        except Exception as error:
            self.logger.exception(
                f"An error occurs during socket connection: {str(error)}")
            sys_exit()

        if self.bot_password:
            self.logger.info(f"Start {self.bot_name} authentication")
            self.send_to_socket(IRC.password(self.bot_password))

        self.send_to_socket(IRC.nick(self.bot_name))

        self.send_to_socket(
            IRC.user(self.bot_name, self.bot_name, self.bot_name, self.bot_name)
        )

        # ----------------------------------------
        #   Start main loop
        # ----------------------------------------

        try:
            # Join channels
            for channel in self.channel_join:
                self.on_join_channel(channel)

            self.logger.info("Listen for messages")
            self.joined_channels = self.channel_join.copy()

            while self.__is_alive:
                messages = self.irc_socket.recv(4096).decode("UTF-8")

                for message in messages.replace('\r', '').splitlines():
                    message = message.strip()

                    if not message:
                        continue

                    for pattern, function in self.__regex:

                        if pattern.match(message) is None:
                            continue

                        results = function(pattern.search(message))
                        if results is None:
                            continue

                        status, *response = results
                        if status is None or not isinstance(status, bool):
                            continue

                        # on_error_request
                        if len(response) == 0:
                            self.__is_alive &= status

                        # on_replies_request (error occurs)
                        elif not status:

                            # Bot related errors are critical
                            if response[0] == self.bot_name:
                                self.__is_alive = False
                                break

                            # Channel related errors only close channel
                            self.quit_channel(response[0])

                            # Ensure to keep the bot alive if some channels are
                            # already joined
                            self.__is_alive &= len(self.joined_channels) > 0
                            break

        except KeyboardInterrupt:
            self.logger.warning("Close by keyboard interrupt")

        except Exception:
            self.logger.exception("An error occurs during execution")

        # ----------------------------------------
        #   Close program
        # ----------------------------------------

        # Close web HTTP listener
        if self.listener_thread is not None \
           and self.listener_thread.is_alive():
            self.logger.info("Close web HTTP listener")
            self.listener_thread.stop()
            self.listener_thread.join()

        # Close thread
        for channel, thread in self.__kick.items():
            if thread is not None:
                thread.cancel()

        # Send exit message when close bot
        for channel in self.joined_channels:
            self.quit_channel(channel)

        try:
            self.logger.info("Close connection")
            self.send_to_socket(IRC.quit(self.__messages["exit"]))

            self.irc_socket.close()

        except Exception:
            self.logger.exception("An error occurs during closing")

    def send_to_socket(self, message):
        """ Sent a message to socket server

        Parameters
        ----------
        message : str
            Message to send to socket server
        """

        if self.irc_socket:
            try:
                self.irc_socket.send(message)

            except BrokenPipeError:
                self.logger.debug(f"Cannot sent message '{message[:-1]}' "
                                  f"caused by a broken pipe")

    def send_message(self, channel, message):
        """ Send a message into socket

        Parameters
        ----------
        channel : str
            Channel where send the message
        message : str
            Message to send
        """

        if not message:
            return

        try:
            if type(message) is not list and type(message) is str:
                message = [message]

            for text in message:

                if text and channel in self.channel_join:
                    self.send_to_socket(IRC.privmsg(channel, text))

                    # Avoid to send the message too quickly
                    sleep(1)

        except Exception as error:
            self.logger.exception(f'An error occurs: {str(error)}')

    def send_private_message(self, user, message):
        """ Send a message into socket

        Parameters
        ----------
        user : str
            User name who going to receive the message
        message : str
            Message to send
        """

        if len(user) == 0 or len(message) == 0:
            return

        # Ensure to always have a list of messages
        if isinstance(message, str):
            message = [message]

        for text in message:
            try:
                if len(text) > 0 and user in self.__users:
                    self.send_to_socket(IRC.privmsg(user, text))

                    # Avoid to send the message too quickly
                    sleep(1)

            except Exception as error:
                self.logger.exception(f'An error occurs: {str(error)}')

    def call_module(self, channel, name , data):
        """ Check if current user message want to call a module

        Parameters
        ----------
        channel : str
            IRC channel name
        name : str
            IRC sender name
        data : str
            IRC message data

        Returns
        -------
        tuple
            Module return status as tuple when an user call a module
        """

        response = None

        # Remove useless spaces ans split message
        data = data.strip().split()
        # Get command name
        command = data[0][len(self.options_character):].lower()

        self.logger.debug(f"'{command}' command request from {name}")

        # Commands
        if command in self.__commands.keys():
            self.__commands[command]["function"](channel, name, data)

        # Modules
        elif command in self.__modules.keys():

            if type(self.__modules[command]) is str:
                command = self.__modules[command]

            module = self.__modules[command][1]

            if module is None:
                return (True, )

            if not hasattr(module, "call"):
                return (True, )

            try:
                module.init(self.logger,
                            channel,
                            name,
                            data,
                            self.informations(channel))

            except Exception:
                self.logger.exception(f"Cannot init module '{command}'")

                self.logger.warning(f"Unload module '{command}'")
                del self.__modules[command]

                return (True, )

            # Check if module need an admin access
            if module.need_admin and name not in self.bot_admin:
                self.send_message(
                    channel, self.__messages["not-admin"])

                return (True, )

            try:
                response = module.call()

                if response is not None:
                    self.send_message(channel, response)
                else:
                    self.send_message(channel, str(module))

            except NotImplementedError:
                pass

            except Exception:
                self.logger.exception(f"Cannot call module '{command}'")

                self.logger.warning(f"Unload module '{command}'")
                del self.__modules[command]

                return (True, )

    def listen_module(self, *args):
        """ Check if current message can be used with modules
        """

        self.manage_module_with_method("listen", *args)

    def user_connection_module(self, *args):
        """ Check if a new user has been registered
        """

        self.manage_module_with_method("connect", *args)

    def manage_module_with_method(self, method, channel, name, data):
        """ Remove non-working modules when calling a specific module method

        Parameters
        ----------
        method : str
            Wakabot module method name
        channel : str
            IRC channel name
        name : str
            IRC sender name
        data : str
            IRC message data
        """

        checked_commands = list()
        modules_to_remove = list()

        for command in self.__modules.keys():

            if isinstance(self.__modules[command], str):
                command = self.__modules[command]

            if command in checked_commands:
                continue

            checked_commands.append(command)

            module = self.__modules[command][1]
            if module is None or not hasattr(module, method):
                continue

            try:
                module.init(self.logger,
                            channel,
                            name,
                            data,
                            self.informations(channel))

            except Exception:
                self.logger.exception(f"Cannot init module '{command}'")
                modules_to_remove.append(command)

                continue

            try:
                self.send_message(channel, getattr(module, method)())

            except NotImplementedError:
                pass

            except Exception:
                self.logger.exception(f"Cannot {method} module '{command}'")
                modules_to_remove.append(command)

        for command in modules_to_remove:
            self.logger.warning(f"Unload module '{command}'")

            del self.__modules[command]

    def on_ping_request(self, message):
        """ Manage ping request

        Parameters
        ----------
        message : _sre.SRE_Match
            Message to parse
        """

        self.send_to_socket(IRC.pong())

    def on_error_request(self, message):
        """ Manage error request

        Parameters
        ----------
        message : _sre.SRE_Match
            Message to parse

        Returns
        -------
        bool
            Main_loop status
        """

        self.logger.error(message.group(1))

        if message.group(1).startswith("Closing Link"):
            return (False, )

        if message.group(1).startswith("Server going down"):
            return (False, )

        return (True, )

    def on_replies_request(self, message):
        """ Manage replies request

        Parameters
        ----------
        message : _sre.SRE_Match
            Message to parse

        Returns
        -------
        bool, str
            Main_loop status and message target name (bot, channel or server)
        """

        code, *target, reason = message.groups()

        try:
            reply = IRC.Replies(int(code))

            if reply == IRC.Replies.ERR_NOMOTD:
                self.logger.warning(
                    "Server's MOTD file could not be opened by the server")

            elif reply.name.startswith('ERR_'):
                self.logger.error(f"({reply.value}) {reason}: {target[-1]}")
                return False, target[-1]

            else:
                self.logger.info(reason)

        except Exception:
            self.logger.info(reason)

        return True, target[-1]

    def on_list_request(self, message):
        """ Manage list request

        Parameters
        ----------
        message : _sre.SRE_Match
            Message to parse
        """

        user, channel, users = message.groups()

        for user in users.split():
            self.register_user(channel, user)

    def on_nick_request(self, message):
        """ Manage kick request

        Parameters
        ----------
        message : _sre.SRE_Match
            Message to parse
        """

        name, new_name = message.groups()

        for channel, users in self.__users.items():

            if name not in users:
                continue

            users[new_name] = users[name]
            del users[name]

        self.logger.info(f"'{name}' has been renamed to '{new_name}'")

    def on_kick_request(self, message):
        """ Manage kick request

        Parameters
        ----------
        message : _sre.SRE_Match
            Message to parse
        """

        user, channel, kicked, reason = message.groups()

        if kicked == self.bot_name:
            self.__users[channel] = dict()

            if self.__auto_rejoin:
                self.__kick[channel] = Timer(
                    self.__kick_timeout,
                    self.on_join_channel,
                    args=[channel])

                self.__kick[channel].start()

        else:
            self.unregister_user(channel, kicked)

    def on_part_request(self, message):
        """ Manage part request

        Parameters
        ----------
        message : _sre.SRE_Match
            Message to parse
        """

        self.unregister_user(*message.groups())

    def on_quit_request(self, message):
        """ Manage quit request

        Parameters
        ----------
        message : _sre.SRE_Match
            Message to parse
        """

        for channel in self.__users.keys():
            self.unregister_user(channel, message.group(1))

    def on_join_request(self, message):
        """ Manage join request

        Parameters
        ----------
        message : _sre.SRE_Match
            IRC message to parse
        """

        user, channel = message.groups()

        # Add a new user to users list
        if not user == self.bot_name:
            self.register_user(channel, user)
            self.user_connection_module(channel, user, user)

        # Show a welcome message on bot channel join
        elif "welcome" in self.__messages.keys():
            welcome = self.__messages["welcome"]
            if not welcome:
                welcome = WakaMetadata.SHORTDESCRIPTION

            self.send_message(channel, welcome)

    def on_join_channel(self, channel, *args):
        """ Join a channel and reset cache

        Parameters
        ----------
        channel : str
            IRC channel name
        """

        if channel in self.channel_join:
            self.__kick[channel] = None

            self.send_to_socket(IRC.join(channel))

            self.logger.info(f"Join channel '{channel}'")

    def on_private_message_request(self, message):
        """ Manage private message request

        Parameters
        ----------
        message : _sre.SRE_Match
            IRC message to parse

        Returns
        -------
        tuple
            Module return status as tuple when an user call a module
        """

        name, channel, data = message.groups()

        # Avoid to listen or send a message for excludes user
        if name in self.modules_exclude_users:
            return (True, )

        # Check if message start with specified command character(s)
        if data.startswith(self.options_character):
            return self.call_module(channel, name, data)

        # Check if the message can be used by a module
        else:
            self.listen_module(channel, name, data)

    def quit_channel(self, channel):
        """ Quit the specified IRC channel and show the quit message

        The QUIT command will be only sent to joined channels

        Parameters
        ----------
        channel : str
            IRC channel name
        """

        if channel in self.joined_channels:
            self.logger.info(f"Quit channel '{channel}'")

            self.send_message(channel, self.__messages["exit"])

            self.joined_channels.remove(channel)

    def register_user(self, channel, name):
        """ Append an user to cached users list

        Parameters
        ----------
        channel : str
            IRC channel name
        name : str
            IRC user name
        """

        if channel not in self.__users:
            self.__users[channel] = dict()

        if name not in self.__users[channel]:
            data = name.split('@')

            if data[-1] not in self.modules_exclude_users:
                self.__users[channel][data[-1]] = len(data) > 1

    def unregister_user(self, channel, name):
        """ Delete an user from cached users list

        Parameters
        ----------
        channel : str
            IRC channel name
        name : str
            IRC user name
        """

        if channel in self.__users and name in self.__users[channel]:
            del self.__users[channel][name]

    def check_valid_token(self, token):
        """ Check if the specified token is a valid one

        This method will use the .tokens file which must be in the same folder
        than the waka.conf file

        The file is reloaded each time to ensure a HTTP request cannot be sent
        with a no longer valid token, without reloading the bot

        Parameters
        ----------
        token : str
            The token to verify

        Returns
        -------
        bool
            True if the token is in the .tokens file, False otherwise
        """

        token_file = self.path.parent.joinpath(".tokens")

        if not token_file.exists():
            return False

        available_tokens = token_file.read_text().splitlines()
        return token in available_tokens

    @needs_administrator_permission
    def command_exit(self, channel, name, data):
        """ Terminate the bot life

        Parameters
        ----------
        channel : str
            IRC channel name
        name : str
            IRC sender name
        data : str
            IRC message data

        Returns
        -------
        tuple or None
            Returns an error tuple if the specified sender is not an admin,
            None otherwise
        """

        self.__is_alive = False

    def command_version(self, channel, name, data):
        """ Show bot version

        Parameters
        ----------
        channel : str
            IRC channel name
        name : str
            IRC sender name
        data : str
            IRC message data
        """

        self.send_message(channel, WakaMetadata.SHORTDESCRIPTION)

    def command_help(self, channel, name, data):
        """ Show help message

        Parameters
        ----------
        channel : str
            IRC channel name
        name : str
            IRC sender name
        data : str
            IRC message data
        """

        # Show every available command
        if len(data) == 1:

            self.send_message(
                channel,
                f"Commands: {', '.join(sorted(self.__commands.keys()))}")

            # Avoid to show aliases
            modules_commands = [
                command for command, value in self.__modules.items()
                if not isinstance(value, str)]

            if len(modules_commands) > 0:
                self.send_message(
                    channel,
                    f"Modules: {', '.join(sorted(modules_commands))}")

        # Show a specific help
        elif len(data) > 1:
            command = data[1]

            if command in self.__commands.keys() and \
               "usage" in self.__commands[command]:

                self.send_message(
                    channel, self.__commands[command]["usage"])

            elif command in self.__modules.keys():
                module = self.__modules[command][1]

                if type(self.__modules[command]) is str:
                    command = self.__modules[command]

                if getattr(module, "help", None) is not None:

                    try:
                        self.send_message(channel, module.help())

                    except Exception:
                        message = f"An error occurs in '{command}'"

                        self.logger.exception(message)

                        self.send_message(channel, message)

                else:
                    self.send_message(
                        channel, "No help available for the specified module")

    @needs_administrator_permission
    def command_reload(self, channel, name, data):
        """ Reload bot modules

        Parameters
        ----------
        channel : str
            IRC channel name
        name : str
            IRC sender name
        data : str
            IRC message data

        Returns
        -------
        tuple or None
            Returns an error tuple if the specified sender is not an admin,
            None otherwise
        """

        self.__init_modules()

    def informations(self, channel):
        """ Retrieve bot information as dict

        Parameters
        ----------
        channel : str
            IRC channel name

        Returns
        -------
        dict
            Bot information
        """

        users = dict()
        if channel in self.__users.keys():
            users = self.__users[channel]

        return {
            "users": users,
            "admins": self.bot_admin,
            "server": self.irc_server,
        }
