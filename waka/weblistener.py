# ------------------------------------------------------------------------------
#  Copyleft 2017-2022  PacMiam
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Python
from base64 import b64decode
from http.server import HTTPServer, CGIHTTPRequestHandler
from json import loads
from threading import Thread


class WakabotHandler(CGIHTTPRequestHandler):

    def check_token(self):
        """ Check the specified Authorization header (if available)

        Returns
        -------
        bool
            True if the specified token exists and is valid, False otherwise
        """

        client_address = self.address_string()

        authorization = self.headers.get("authorization", None)
        if authorization is None:
            self.server.bot.logger.warning(
                f"The request from {client_address} did not have the "
                f"Authorization header")
            return False

        if not authorization.lower().startswith("bearer "):
            self.server.bot.logger.warning(
                f"The request from {client_address} did not have the "
                "Bearer token in Authorization header")
            return False

        try:
            auth_type, value = authorization.split()

            is_valid = self.server.bot.check_valid_token(
                b64decode(value).decode("utf-8"))

            if not is_valid:
                self.server.bot.logger.warning(
                    f"The token used by {client_address} is not valid")

            return is_valid

        except Exception as error:
            self.server.bot.logger.error(error)
            return False

    def retrieve_data(self):
        """ Retrieve data from request content based on headers values

        The data received by the web HTTP listener needs to be send as JSON

        Returns
        -------
        dict or None
            The received JSON data parsed as dict, None otherwise
        """

        if not self.headers.get("content-type") == "application/json":
            return None

        content_length = int(self.headers.get("content-length", 0))
        if content_length == 0:
            return None

        try:
            data = self.rfile.read(content_length).decode("utf-8")
            return loads(data)

        except Exception as error:
            self.server.bot.logger.error(error)
            return None

    def do_GET(self):
        """ Ignore GET request by sending a 404 HTTP error
        """

        self.send_response(404)
        self.end_headers()

    def do_POST(self):
        """ Manage POST requests
        """

        if self.check_token():
            data = self.retrieve_data()

            if data is not None:
                channel = data.get("channel", None)
                message = data.get("message", None)

                if channel is not None and message is not None:
                    self.server.bot.send_message(channel, message)

                    self.send_response(200)
                    self.end_headers()

                else:
                    self.send_response(400)
                    self.end_headers()

            else:
                self.send_response(400)
                self.end_headers()

        else:
            self.send_response(403)
            self.end_headers()


class WakabotHTTPServer(HTTPServer):

    def __init__(self, bot, port):
        """ Constructor

        Parameters
        ----------
        bot : waka.bot.Bot
            The WakaBot instance used to send message to IRC socket
        port : int
            The web HTTP server port
        """

        super().__init__(("127.0.0.1", port), WakabotHandler)

        self.bot = bot


class WakabotHTTPServerThread(Thread):

    def __init__(self, bot, port):
        """ Constructor

        Parameters
        ----------
        bot : waka.bot.Bot
            The WakaBot instance used to send message to IRC socket
        port : int
            The web HTTP server port
        """

        super().__init__()

        self.server = WakabotHTTPServer(bot, port)
        self.name = "HTTPThread"

    def run(self):
        """ Run the HTTP server when the thread is started
        """

        self.server.serve_forever()

    def stop(self):
        """ Stop the HTTP server properly
        """

        self.server.shutdown()
