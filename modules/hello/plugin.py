# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Random
from random import randint

# Regex
from re import match

# Waka
from waka.module import WakaModule


class Module(WakaModule):

    def listen(self):
        """ Listen messages from channels

        Returns
        -------
        str
            Response message
        """

        result = match(r'^([^\s]+)\s+([^$]+)$', self.response.lower().strip())

        if result is not None and self.bot_name.lower() in result.group(2):

            if result.group(1) in self.variable_welcome.lower().split():
                welcome = self.variable_welcome.split()

                return ' '.join(
                    (welcome[randint(0, len(welcome) - 1)], self.current_user))

        return str()
