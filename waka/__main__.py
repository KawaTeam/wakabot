# ------------------------------------------------------------------------------
#  Copyleft 2017-2021  PacMiam
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Logging
from logging import getLogger

# System
from argparse import ArgumentParser

# Waka
from waka import WakaMetadata
from waka.bot import Bot


# ------------------------------------------------------------------------------
#   Launcher
# ------------------------------------------------------------------------------

def main():
    """ Main launcher
    """

    # Generate default arguments
    parser = ArgumentParser(prog=WakaMetadata.NAME,
                            description=WakaMetadata.DESCRIPTION,
                            epilog=WakaMetadata.EPILOG,
                            conflict_handler='resolve')

    parser.add_argument('-v',
                        '--version',
                        action='version',
                        version=WakaMetadata.SHORTDESCRIPTION,
                        help='show the current version')

    parser.add_argument('-d',
                        '--debug',
                        action='store_true',
                        help='activate debug flag')

    parser.add_argument('-c',
                        '--configuration',
                        type=str,
                        action='store',
                        metavar='FILE',
                        default='waka.conf',
                        help='set the main configuration')

    arguments = parser.parse_args()

    try:
        bot = Bot(**vars(arguments))
        bot.main_loop()

    except KeyboardInterrupt:
        getLogger(__name__).warning('Close by keyboard interrupt')

    except Exception as error:
        getLogger(__name__).exception(error)


if __name__ == '__main__':
    main()
