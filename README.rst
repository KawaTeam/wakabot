WakaBot
=======
WakaBot is a little IRC bot writing in Python 3.6 without any external
libraries.

Dependencies
------------
WakaBot only needs the following dependencies to work:

- Python 3.6

Configuration
-------------
The first step, is to make a copy of the default configuration file
::

    cp documentation/default.waka.conf waka.conf

More informations on configuration options in documentation/README.rst

Installation
------------
To install the wakabot, use ``pip``
::

    pip install .

Execute
-------
To execute the bot, you can use this command
::

    wakabot -c waka.conf
