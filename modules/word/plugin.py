# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Regex
from re import match, sub, IGNORECASE

# Waka
from waka.module import WakaModule


class Module(WakaModule):

    CHARACTERS = r'[,;:\.!\?\-_…]'

    def __init__(self, *args, **kargs):
        """ Constructor
        """

        super().__init__(*args, **kargs)

        self.words = self.variable_words.split()

        # Initialize words counter
        for word in self.words:
            if not self.config.has_section(word):
                self.config.add_section(word)
                self.config.set(word, "count", '0')

        # Save modification
        self.config.save()

    def call(self):
        """ Call this method from channels

        Returns
        -------
        str or None
            Response message
        """

        # List available words counter
        if len(self.response) == 1:
            texts = list()

            for word in self.words:
                texts.append(self.message_show % {
                    'word': word,
                    'count': self.config.getint(word, 'count')
                })

            return ', '.join(texts)

        # List a specific word counter
        elif self.response[1] in self.words:
            return self.message_show % {
                'word': self.response[1],
                'count': self.config.getint(self.response[1], 'count')
            }

    def listen(self):
        """ Listen messages from channels

        Returns
        -------
        str
            Response message
        """

        if len(self.response) > 1:
            clean_message = self.response.lower().strip()

            for element in clean_message.split():
                for word in self.words:
                    cleanup = sub(self.CHARACTERS, '', element).strip()
                    if match(fr'\b{word}\b', cleanup, IGNORECASE) is None:
                        continue

                    counter = self.config.getint(word, 'count', fallback=int())
                    if not self.config.has_section(word):
                        self.config.add_section(word)

                    self.config.set(word, 'count', str(counter + 1))

            self.config.save()
