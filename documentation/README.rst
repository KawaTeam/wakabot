Configuration options
=====================

irc
---

server
~~~~~~
IRC server hostname or IP address.

- **Type**: String
- **Default**: chat.freenode.net

port
~~~~
IRC server port.

- **Type**: Number
- **Default**: 6667

use-ssl
~~~~~~~
Use an SSL socket.

- **Type**: Boolean
- **Default**: yes

channel
-------

join
~~~~
Channels to join during startup.

- **Type**: Strings separated with spaces
- **Default**:

bot
---

name
~~~~
Bot name.

- **Type**: String
- **Default**: Bot

password
~~~~~~~~
Password using to authentificate user on IRC server.

- **Type**: String
- **Default**:

admin
~~~~~
Administrators list who can manage the bot.

- **Type**: Strings separated with spaces
- **Default**:

path
----

log
~~~~
Logging file path.

- **Type**: String
- **Default**: waka.log

modules
~~~~~~~
Modules path.

- **Type**: Strings separated with spaces
- **Default**:

options
-------

kick-timeout
~~~~~~~~~~~~
Time to wait before auto-rejoin a kicked channel (in seconds).

- **Type**: Number
- **Default**: 10

auto-rejoin
~~~~~~~~~~~
Allow the bot to auto-rejoin kicked channel.

- **Type**: Boolean
- **Default**: no

character
~~~~~~~~~
Character using to call command.

- **Type**: String
- **Default**: .

modules
-------

exclude-users
~~~~~~~~~~~~~
Usernames to exclude from users list.

- **Type**: Strings separated with spaces
- **Default**: ChanServ

ignores
~~~~~~~
Modules ignored during startup.

- **Type**: Strings separated with spaces
- **Default**:

messages
--------

welcome
~~~~~~~
Message which appear when bot enter a channel.

- **Type**: String
- **Default**: Welcome

exit
~~~~
Message which appear when bot leave a channel.

- **Type**: String
- **Default**: Quit

not-admin
~~~~~~~~~
Message which appear when a non-admin user try a privilege command.

- **Type**: String
- **Default**: You are not allowed to do this command
