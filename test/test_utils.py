# ------------------------------------------------------------------------------
#  Copyleft 2017-2021  PacMiam
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Unittest
import unittest

# Wakabot
from waka.utils import Version


# ------------------------------------------------------------------------------
#   Tests
# ------------------------------------------------------------------------------

class VersionTestCase(unittest.TestCase):

    def setUp(self):
        """ Initialize each test with some data
        """

        self.version = Version("0.1.0")

    def test_constructor(self):
        """ Version object constructor test
        """

        with self.assertRaises(ValueError):
            Version('')

        with self.assertRaises(ValueError):
            Version("0.1")

        Version("0.0.1")
        Version("0.1.0")
        Version("1.0.0")

    def test_to_str(self):
        """ Strong representation test
        """

        self.assertTrue(str(self.version) == "0.1.0")

    def test_compare_lt(self):
        """ Lesser than comparison test
        """

        self.assertFalse(self.version < Version("0.1.0"))
        self.assertTrue(self.version < Version("0.2.0"))
        self.assertFalse(self.version < Version("0.0.5"))
        self.assertTrue(self.version < Version("1.0.0"))
        self.assertTrue(self.version < Version("0.10.0"))

    def test_compare_le(self):
        """ Lesser or equal than comparison test
        """

        self.assertTrue(self.version <= Version("0.1.0"))
        self.assertTrue(self.version <= Version("0.2.0"))
        self.assertFalse(self.version <= Version("0.0.5"))
        self.assertTrue(self.version <= Version("1.0.0"))
        self.assertTrue(self.version <= Version("0.10.0"))

    def test_compare_gt(self):
        """ Greater than comparison test
        """

        self.assertFalse(self.version > Version("0.1.0"))
        self.assertFalse(self.version > Version("0.2.0"))
        self.assertTrue(self.version > Version("0.0.5"))
        self.assertFalse(self.version > Version("1.0.0"))
        self.assertFalse(self.version > Version("0.10.0"))

    def test_compare_ge(self):
        """ Greater or equal than comparison test
        """

        self.assertTrue(self.version >= Version("0.1.0"))
        self.assertFalse(self.version >= Version("0.2.0"))
        self.assertTrue(self.version >= Version("0.0.5"))
        self.assertFalse(self.version >= Version("1.0.0"))
        self.assertFalse(self.version >= Version("0.10.0"))
