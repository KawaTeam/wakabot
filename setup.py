#!/usr/bin/env python3
# ------------------------------------------------------------------------------
#  Copyleft 2017-2021  PacMiam
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

from setuptools import setup
from setuptools import find_packages

setup(
    name='Wakabot',
    version='0.4.0',
    author='PacMiam',
    author_email='pacmiam@tuxfamily.org',
    description='WakaBot is a little IRC bot writing in Python 3.6 without '
                'any external libraries',
    keywords='irc bot',
    url='https://framagit.org/KawaTeam/wakabot/',
    project_urls={
        'Source': 'https://framagit.org/KawaTeam/wakabot',
        'Tracker': 'https://framagit.org/KawaTeam/wakabot/issues',
        'Modules': 'https://framagit.org/KawaTeam/wakabot-extra',
    },
    packages=find_packages(exclude=['tools', 'test']),
    include_package_data=True,
    python_requires='~= 3.6',
    entry_points={
        'console_scripts': [
            'wakabot = waka.__main__:main',
        ],
    },
    extras_require={
        'dev': [
            'pytest',
            'flake8',
        ],
    },
    license='GPLv3',
    classifiers=[
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3.6',
    ],
)
