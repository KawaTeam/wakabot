# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Random
from random import randint

# Regex
from re import compile as re_compile

# Waka
from waka.module import WakaModule


class Module(WakaModule):

    def call(self):
        """ Call this method from channels

        Returns
        -------
        str or None
            Response message
        """

        if len(self.response) > 1:
            regex = re_compile(r'(\d*)d(\d+)')

            if regex.match(self.response[1]):
                result = regex.search(self.response[1])

                number = 1
                if len(result.group(1)) > 0:
                    number = int(result.group(1))

                dice = int(result.group(2))

                # ----------------------------------------
                #   Check
                # ----------------------------------------

                if number > self.variable_max_dices:
                    return self.message_wrong_max_dices.format(
                        self.variable_max_dices)

                elif number < self.variable_min_dices:
                    return self.message_wrong_min_dices.format(
                        self.variable_min_dices)

                elif dice > self.variable_max_faces:
                    return self.message_wrong_max_faces.format(
                        self.variable_max_faces)

                elif dice < self.variable_min_faces:
                    return self.message_wrong_min_faces.format(
                        self.variable_min_faces)

                # ----------------------------------------
                #   Launch
                # ----------------------------------------

                dices_rolls = list()
                dices_result = int()

                for index in range(number):
                    roll = randint(1, dice)

                    dices_rolls.append(str(roll))
                    dices_result += roll

                # ----------------------------------------
                #   Message
                # ----------------------------------------

                if len(dices_rolls) > 1:
                    if len(dices_rolls) > 10:
                        dices_rolls = dices_rolls[:10]
                        dices_rolls.append('…')

                    return f"{dices_result} [{', '.join(dices_rolls)}]"

                return str(dices_result)

        return None
