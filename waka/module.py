# ------------------------------------------------------------------------------
#  Copyleft 2017-2021  PacMiam
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Codec
from codecs import open as codecs_open

# Configuration
from configparser import ConfigParser

# Filesystem
from pathlib import Path

# Regex
from re import match


# ------------------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------------------

class WakaModule(object):
    """ Module template
    """

    def __init__(self, bot_name, character, module_path):
        """ Constructor

        Parameters
        ----------
        bot_name : str
            Bot name
        character : str
            Call character
        module_path : str
            Absolute path to module

        Raises
        ------
        ValueError
            When the plugin module_path is missing
        OSError
            When the module_path not exists
        """

        if not issubclass(type(module_path), Path):
            raise ValueError("Module cannot be loaded: Missing module_path")

        if not module_path.exists():
            raise FileNotFoundError(
                "Cannot found plugin file path on filesystem")

        # ----------------------------------------
        #   Initialize variables
        # ----------------------------------------

        self.__path = module_path.parent

        self.__user = str()
        self.__bot_name = bot_name
        self.__character = character

        # ----------------------------------------
        #   Initialize module paths
        # ----------------------------------------

        # Plugin metadata
        self.__manifest = self.__path.joinpath('manifest.conf')

        if not self.__manifest.exists():
            raise OSError(2, 'Cannot found manifest.conf')

        # Plugin configuration
        self.__plugin = self.__path.joinpath('plugin.conf')

        # Plugin internal data
        self.__data = self.__path.joinpath('plugin.data')

        # ----------------------------------------
        #   Configuration
        # ----------------------------------------

        self.__config = WakaConfigParser(self.__manifest)
        self.__config.read()

        options = {
            'plugin': {
                'name': str(),
                'author': str(),
                'version': str(),
                'description': str(),
                'operator-only': False,
            },
            'command': {
                'name': str(),
                'aliases': list(),
                'parameters': str(),
            },
        }

        for section, values in options.items():

            for key, default in values.items():

                if type(default) is bool:
                    value = self.__config.getboolean(
                        section, key, fallback=default)

                elif type(default) is list:
                    value = self.__config.get(
                        section, key, fallback=str()).split()

                else:
                    value = self.__config.get(section, key, fallback=default)

                setattr(self, f"conf_{section}_{key.replace('-', '_')}", value)

        # ----------------------------------------
        #   Module variables
        # ----------------------------------------

        self.__plugin_config = WakaConfigParser(self.__plugin)
        self.__plugin_config.read()

        if self.__plugin_config.has_section('variables'):

            for key, value in self.__plugin_config.items('variables'):

                if match(r'^\d+$', value):
                    value = int(value)

                elif match(r'^\d+\.\d+$', value):
                    value = float(value)

                elif value.lower in ('true', 'yes', 'on'):
                    value = True

                elif value.lower in ('false', 'no', 'off'):
                    value = False

                setattr(self, f"variable_{key.replace('-', '_')}", value)

        if self.__plugin_config.has_section('messages'):

            for key in self.__plugin_config.options('messages'):
                value = self.__plugin_config.get(
                    'messages', key, fallback=str(), raw=True)

                setattr(self, f"message_{key.replace('-', '_')}", value)

        # ----------------------------------------
        #   Module data
        # ----------------------------------------

        self.__plugin_data = WakaConfigParser(self.__data)
        self.__plugin_data.read()

    def __str__(self):
        """ Formated informations

        Returns
        -------
        str
            Formated string
        """

        if self.conf_command_parameters:
            arguments = ' '.join((self.conf_command_name,
                                  self.conf_command_parameters))

            return f'Usage: {arguments}'

        return self.conf_command_name

    def help(self):
        """ Show module informations

        Returns
        -------
        list
            Informations as strings list
        """

        texts = [self.conf_plugin_description]

        if self.conf_command_parameters:
            arguments = ' '.join((self.conf_command_name,
                                  self.conf_command_parameters))

            texts.append(f'Usage: {arguments}')

        if len(self.conf_command_aliases) > 0:
            texts.append(f"Aliases: {', '.join(self.conf_command_aliases)}")

        return texts

    def init(self, logger, channel, user, messages, informations):
        """ Intialize variables

        Parameters
        ----------
        logger : object
            Logger instance used to send message in the log file
        channel : str
            Channel name where the message come from
        user : str
            User name who send the message
        messages : list
            Message as string list
        informations : dict
            Bot informations
        """

        self.__logger = logger

        self.__user = user
        self.__channel = channel
        self.__messages = messages

        for key, value in informations.items():
            setattr(self, f"info_{key.replace('-', '_')}", value)

    def call(self):
        """ Module event called when a command was asked by any user

        Returns
        -------
        list or str or None
            Module response as list of strings, string or null
        """

        raise NotImplementedError()

    def joined(self):
        """ Module event called when a new user joined a channel
        """

        raise NotImplementedError()

    def listen(self):
        """ Module event called when a message was received by the bot

        Returns
        -------
        list or str or None
            Module response as list of strings, string or null
        """

        raise NotImplementedError()

    def unjoined(self):
        """ Module event called when a new user quit a channel
        """

        raise NotImplementedError()

    def get_module_path(self):
        """ Retrieve plugin path

        Returns
        -------
        str
            Addon path
        """

        return self.__path

    def get_manifest_path(self):
        """ Retrieve plugin manifest path

        Returns
        -------
        str
            Addon manifest path
        """

        return self.__manifest

    def get_internal_file(self, *args):
        """ Retrieve a file from module directory

        Returns
        -------
        pathlib.Path
            Found file path, None otherwise
        """

        path = self.__path.joinpath(*args)

        if path.exists():
            return path

        return None

    @property
    def config(self):
        """ Retrieve config

        Returns
        -------
        str
            Current user
        """

        return self.__plugin_data

    @property
    def current_user(self):
        """ Retrieve current user name

        Returns
        -------
        str
            Current user
        """

        return self.__user

    @property
    def current_channel(self):
        """ Retrieve current channel name

        Returns
        -------
        str
            Current channel
        """

        return self.__channel

    @property
    def users(self):
        """ Retrieve current channel users list

        Returns
        -------
        list
            Users name list
        """

        if not hasattr(self, "info_users"):
            return list()

        return list(self.info_users.keys())

    @property
    def bot_name(self):
        """ Retrieve bot name

        Returns
        -------
        str
            Bot name
        """

        return self.__bot_name

    @property
    def response(self):
        """ Retrieve messages

        Returns
        -------
        list
            Messages
        """

        return self.__messages

    @property
    def logger(self):
        """ Retrieve logger instance

        Returns
        -------
        list
            Messages
        """

        return self.__logger

    @property
    def name(self):
        """ Retrieve plugin name

        Returns
        -------
        str
            Addon name
        """

        return self.conf_plugin_name

    @property
    def author(self):
        """ Retrieve plugin author

        Returns
        -------
        str
            Addon author
        """

        return self.conf_plugin_author

    @property
    def version(self):
        """ Retrieve plugin version

        Returns
        -------
        str
            Addon version
        """

        return self.conf_plugin_version

    @property
    def description(self):
        """ Retrieve plugin description

        Returns
        -------
        list
            Addon description
        """

        return self.conf_plugin_description

    @property
    def need_admin(self):
        """ Retrieve plugin operator only status

        Returns
        -------
        bool
            Addon operator status
        """

        return self.conf_plugin_operator_only


class WakaConfigParser(ConfigParser):

    def __init__(self, path, *args, **kwargs):
        """ Constructor

        Parameters
        ----------
        path : pathlib.Path or str
            Configuration file path
        """

        super().__init__(*args, **kwargs)

        self.__path = path
        if type(path) is str:
            self.__path = Path(path).resolve()

    def read(self):
        """ Read configuration file content
        """

        if self.__path.exists():
            self.read_file(codecs_open(self.__path, 'r', 'utf8'))

    def save(self):
        """ Save new options data into configuration file
        """

        with self.__path.open('w') as pipe:
            self.write(pipe)

        self.read()
