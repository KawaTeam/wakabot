# ------------------------------------------------------------------------------
#  Copyleft 2017-2021  PacMiam
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Regex
from re import search


# ------------------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------------------

class Version():
    """ Generic class to manage version string like X.Y.Z
    """

    def __init__(self, version, *args, **kwargs):
        """ Constructor

        Parameters
        ----------
        version : str
            String to parse to retrieve version information
        """

        if not version:
            raise ValueError("Cannot use an empty string as version")

        result = search(r"(\d+)\.(\d+)\.(\d+)", version)

        if result is None:
            raise ValueError(f"String '{version}' did not follow version "
                             f"pattern 'X.Y.Z'")

        self.major = int(result.group(1))
        self.minor = int(result.group(2))
        self.revision = int(result.group(3))

    def __str__(self):
        """ String to represent class instance
        """

        return f"{self.major}.{self.minor}.{self.revision}"

    def __lt__(self, second):
        """ Check if object instance is lesser than second object

        Parameters
        ----------
        second : waka.utils.Version
            Version object to compare

        Returns
        -------
        bool
            Comparison results

        Raises
        ------
        TypeError
            When second parameter is not a Version object
        """

        if not isinstance(second, Version):
            raise TypeError("Comparison function needs a Version object")

        if self.major < second.major:
            return True

        if self.major > second.major:
            return False

        if self.minor < second.minor:
            return True

        if self.minor > second.minor:
            return False

        return self.revision < second.revision

    def __le__(self, second):
        """ Check if object instance is lesser or equal than second object

        Parameters
        ----------
        second : waka.utils.Version
            Version object to compare

        Returns
        -------
        bool
            Comparison results

        Raises
        ------
        TypeError
            When second parameter is not a Version object
        """

        if not isinstance(second, Version):
            raise TypeError("Comparison function needs a Version object")

        if self.major < second.major:
            return True

        if self.major > second.major:
            return False

        if self.minor < second.minor:
            return True

        if self.minor > second.minor:
            return False

        return self.revision <= second.revision

    def __gt__(self, second):
        """ Check if object instance is greater than second object

        Parameters
        ----------
        second : waka.utils.Version
            Version object to compare

        Returns
        -------
        bool
            Comparison results

        Raises
        ------
        TypeError
            When second parameter is not a Version object
        """

        if not isinstance(second, Version):
            raise TypeError("Comparison function needs a Version object")

        if self.major > second.major:
            return True

        if self.major < second.major:
            return False

        if self.minor > second.minor:
            return True

        if self.minor < second.minor:
            return False

        return self.revision > second.revision

    def __ge__(self, second):
        """ Check if object instance is greater or equal than second object

        Parameters
        ----------
        second : waka.utils.Version
            Version object to compare

        Returns
        -------
        bool
            Comparison results

        Raises
        ------
        TypeError
            When second parameter is not a Version object
        """

        if not isinstance(second, Version):
            raise TypeError("Comparison function needs a Version object")

        if self.major > second.major:
            return True

        if self.major < second.major:
            return False

        if self.minor > second.minor:
            return True

        if self.minor < second.minor:
            return False

        return self.revision >= second.revision

    def __eq__(self, second):
        """ Check if object instance is equal to second object

        Parameters
        ----------
        second : waka.utils.Version
            Version object to compare

        Returns
        -------
        bool
            Comparison results

        Raises
        ------
        TypeError
            When second parameter is not a Version object
        """

        if not isinstance(second, Version):
            raise TypeError("Comparison function needs a Version object")

        return (self.major == second.major and
                self.minor == second.minor and
                self.revision == second.revision)
