# ------------------------------------------------------------------------------
#  Copyleft 2017-2021  PacMiam
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Filesystem
from pathlib import Path

# Unittest
import unittest

# Wakabot
from waka.module import WakaModule, WakaConfigParser


# ------------------------------------------------------------------------------
#   Tests
# ------------------------------------------------------------------------------

class ModuleTestCase(unittest.TestCase):

    def setUp(self):
        """ Initialize each test with some data
        """

        self.bot_name = "Wakabot"
        self.message = "Message received by module"
        self.module_path = Path("modules", "hello", "manifest.conf").resolve()

        self.module = WakaModule(self.bot_name, '.', self.module_path)
        self.module.init(
            None,
            self.bot_name,
            self.bot_name,
            self.message,
            {
                "users": {
                    self.bot_name: True,
                },
                "admins": [
                    self.bot_name,
                ],
                "server": "localhost",
            })

        self.config = WakaConfigParser(self.module_path)
        self.config.read()

    def test_plugin_metadata(self):
        """ Module metadata.conf parser test
        """

        for variable in ("author", "description", "name", "version"):
            attribute = getattr(self.module, variable, None)

            self.assertIsNotNone(attribute)

            self.assertEqual(attribute, self.config.get("plugin", variable))

        self.assertIsInstance(self.module.need_admin, bool)

        self.assertEqual(self.module.need_admin,
                         self.config.getboolean("plugin", "operator-only"))

    def test_attributes(self):
        """ Module attributes test
        """

        self.assertEqual(self.module.bot_name, self.bot_name)
        self.assertEqual(self.module.current_channel, self.bot_name)
        self.assertEqual(self.module.current_user, self.bot_name)
        self.assertEqual(self.module.response, self.message)

        self.assertIsNone(self.module.logger)

        result = self.module.users
        self.assertIsInstance(result, list)
        self.assertEqual(result, [self.bot_name])

        result = self.module.help()
        self.assertIsInstance(result, list)
        self.assertEqual(result, [self.config.get("plugin", "description")])
